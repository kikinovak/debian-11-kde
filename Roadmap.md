# Debian 11 KDE Roadmap

Components of my "perfect" custom Linux desktop with bells and whistles based
on Debian 11 *Bullseye* and KDE.


## Base system

* [X] Base system (Legacy BIOS)

* [X] Base system (UEFI)

* [X] Custom Bash configuration for `root`

* [X] Custom Bash configuration for users

* [X] Custom Vim configuration

* [X] SSH access for `root`

* [X] No timeout for `sudo`

* [X] Official package repositories

* [ ] Third-party package repositories

* [ ] Flatpak repositories

* [X] Command-line utilities: `links`, `nmap`, `speedtest`, etc.

* [ ] Custom GRUB configuration

* [ ] Startup messages & console resolution

* [ ] Disable CPU mitigations


## X Window System

* [X] Basic X11 server

* [X] Lightweight window manager: WindowMaker

* [X] Custom Xterm configuration

* [ ] NVidia video card drivers


## Fonts

* [ ] Selection of free TrueType fonts

* [ ] Classic Microsoft fonts (Windows XP)

* [ ] Additional Microsoft fonts (Windows Vista)

* [ ] Eurostile fonts

* [ ] Anti-aliasing


## Desktop environment

* [ ] Minimal Plasma desktop

* [ ] SDDM login manager

* [ ] Decent default theme: Breeze or similar

* [ ] Unified Qt/GTK theme

* [ ] Default taskbar launchers

* [ ] Default wallpaper

* [ ] Remove unused applications

* [ ] Custom desktop configuration

* [ ] Wallpaper collection

* [ ] Custom menu entries

* [ ] Network configuration: NetworkManager


## Internet

* [ ] Web browser: Mozilla Firefox ESR

* [ ] Ad blocker: uBlock Origin

* [ ] Web browser: Chromium

* [ ] Mail client: Mozilla Thunderbird

* [ ] BitTorrent client: Transmission

* [ ] Network storage: OwnCloud

* [ ] FTP client: Filezilla

* [ ] Instant messenger: Pidgin

* [ ] Citrix client: Citrix Workspace App

* [ ] Electronic ID cards: eID Viewer


## Office

* [ ] Office suite: LibreOffice

* [ ] Translation memory: OmegaT


## Graphics

* [ ] Image viewer: Gwenview

* [ ] Document viewer: Okular

* [ ] Photo management: Digikam

* [ ] Scanner software: SimpleScan

* [ ] Screenshot utility: Spectacle

* [ ] Image manipulation: GIMP

* [ ] Photo manipulation: Darktable

* [ ] Vector graphics: Inkscape

* [ ] Desktop publishing: Scribus

* [ ] Jewel case covers: Gtkcdlabel


## Multimédia

* [ ] Audio player: Audacious

* [ ] Video player: VLC

* [ ] Audio editor: Audacity

* [ ] Video editor: Kdenlive

* [ ] Webcam application: Guvcview

* [ ] Screencast application: Vokoscreen

* [ ] Multimedia converter: WinFF


## System

* [ ] Terminal: Konsole

* [ ] Monitoring: System Monitor

* [ ] System information: KInfoCenter

* [ ] Remote desktop: KRDC

* [ ] HP printers: HPLIP

* [ ] Virtualization: VirtualBox

* [ ] Virtualization: Virtual Machine Manager

* [ ] Remote maintenance: AnyDesk

* [ ] Power management: TLP


## Utilities

* [ ] Text editor: Kate

* [ ] Calculator: KCalc

* [ ] Password management: KeePassXC

* [ ] Wallet management: KWalletManager

* [ ] CD/DVD burning: K3B 

* [ ] Archiving tool: Ark

* [ ] Character selector: KCharSelect

* [ ] Android phones: KDE Connect

* [ ] Mind Mapping: View Your Mind

* [ ] Hierarchical notes: CherryTree

* [ ] Desktop search: Recoll


## Games

* [ ] Solitaire

* [ ] Steam

