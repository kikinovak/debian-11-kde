#!/bin/bash
#
# setup.sh
#
# (c) Niki Kovacs 2022 <info@microlinux.fr>

# Current directory
CWD=$(pwd)

# Default to english for admin tasks
LANG=en_US.UTF-8
export LANG

# Slow things down a bit
SLEEP=1

# Logs
LOG="/var/log/$(basename "${0}" .sh).log"
echo >> ${LOG}
echo "***** $(date) *****" >> ${LOG}
echo >> ${LOG}

# Defined users
USERS="$(awk -F: '$3 > 999 && $3 < 65534 {print $1}' /etc/passwd | sort)"

# Basic tools
TOOLS=$(grep -E -v '(^\#)|(^\s+$)' ${CWD}/pkgs/tools.txt)

# Make sure the script is being executed with superuser privileges.
if [[ "${UID}" -ne 0 ]]
then

  echo
  echo "  Please run with sudo or as root." >&2
  echo
  sleep ${SLEEP}

  exit 1

fi

# Make sure we're running Debian 11 Bullseye
source /etc/os-release

if [ "${?}" -ne 0 ]
then

  echo 
  echo "  Unsupported operating system." >&2
  echo
  sleep ${SLEEP}

  exit 1

elif [ "${PRETTY_NAME}" != "Debian GNU/Linux 11 (bullseye)" ] 
then

  echo 
  echo "  Please run this script on Debian 11 Bullseye only." >&2
  echo
  sleep ${SLEEP}

  exit 1

fi

echo 
echo "  ####################################"
echo "  # Debian 11 Bullseye configuration #" 
echo "  ####################################"
echo
sleep ${SLEEP}

usage() {

  # Display help message
  echo "  Usage: ${0} OPTION"
  echo
  echo "  Debian 11 Bullseye post-install configuration."
  echo
  echo "  Options:"
  echo "    --shell    Configure Bash, Vim, locales, SSH and sudo."
  echo "    --repos    Setup official and third-party repositories."
  echo "    --tools    Install full set of command-line tools."
  echo "    --xorg     Install X11 and Window Maker."
  echo
  echo "  Logs are written to ${LOG}."
  echo
  sleep ${SLEEP}

}

configure_shell() {

  echo "  === Shell configuration ==="
  echo
  sleep ${SLEEP}

  echo "  Configuring Bash shell for user: root"
  cp -vf ${CWD}/bash/root-bashrc /root/.bashrc >> ${LOG}
  sleep ${SLEEP}

  echo "  Configuring Bash shell for future users."
  cp -vf ${CWD}/bash/user-bashrc /etc/skel/.bashrc >> ${LOG}
  sleep ${SLEEP}

  echo "  Configuring Vim editor."
  sed -i -e 's/" let g/let g/g' /etc/vim/vimrc
  cp -vf ${CWD}/vim/vimrc.local /etc/vim/ >> ${LOG}
  sleep ${SLEEP}

  if [ ! -z "${USERS}" ]
  then

    for USER in ${USERS}
    do

      if [ -d /home/${USER} ]
      then

        echo "  Configuring Bash shell for user: ${USER}"
        cp -vf ${CWD}/bash/user-bashrc /home/${USER}/.bashrc >> ${LOG}
        chown -v ${USER}:${USER} /home/${USER}/.bashrc >> ${LOG}
        sleep ${SLEEP}

      fi

    done

  fi

  echo "  Generating locales."
  sed -i "s/# de_AT.UTF-8 UTF-8/de_AT.UTF-8 UTF-8/g" /etc/locale.gen
  sed -i "s/# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/g" /etc/locale.gen
  sed -i "s/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g" /etc/locale.gen
  sed -i "s/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/g" /etc/locale.gen
  locale-gen >> ${LOG}

  if [ ! -d /etc/ssh/sshd_config.d ]
  then
    echo "  Installing package: openssh-server"
    apt-get update >> ${LOG}
    apt-get install -y openssh-server >> ${LOG}
  fi
     
  echo "  Configuring SSH server."
  cp -vf ${CWD}/sshd/custom.conf /etc/ssh/sshd_config.d/ >> ${LOG}
  sed -i -e '/AcceptEnv/s/^#\?/#/' /etc/ssh/sshd_config
  systemctl reload sshd
  sleep ${SLEEP}

  if [ ! -d /etc/sudoers.d ]
  then
    echo "  Installing package: sudo"
    apt-get update >> ${LOG}
    apt-get install -y sudo >> ${LOG}
  fi

  echo "  Configuring persistent password for sudo."
  cp -vf ${CWD}/sudoers.d/persistent_password /etc/sudoers.d/ >> ${LOG}
  sleep ${SLEEP}

echo

}

configure_repos() {

  echo "  === Package repository configuration ==="
  echo
  sleep ${SLEEP}

  echo "  Removing existing repositories."
  rm -f /etc/apt/sources.list.d/*.list
  sleep ${SLEEP}

  echo "  Configuring official repositories."
  cp -vf ${CWD}/apt/sources.list /etc/apt/ >> ${LOG}
  sleep ${SLEEP}

  echo

}

install_tools() {

  echo "  === Install basic system tools ==="
  echo
  sleep ${SLEEP}

  echo "  Updating information about available packages."
  apt-get update >> ${LOG}

  for PACKAGE in ${TOOLS}
  do

    if ! dpkg-query --status ${PACKAGE} 2> /dev/null | \
      grep "install ok installed" > /dev/null
    then

      echo "  Installing package: ${PACKAGE}"
      apt-get install -y ${PACKAGE} >> ${LOG} 2>&1

    fi

  done

  echo "  Basic system tools installed on the system."
  echo
  sleep ${SLEEP}

}

install_wmaker() {

  echo "  === Install X11 and Window Maker ==="
  echo
  sleep ${SLEEP}

  echo "  Installing X Window System."
  apt-get install -y xorg mesa-utils nvidia-detect >> ${LOG} 2>&1
  sleep ${SLEEP}

  echo "  Installing Window Maker."
  apt-get install -y wmaker >> ${LOG} 2>&1
  sleep ${SLEEP}

  echo "  Configuring Window Maker for future users."
  rm -rf /etc/skel/GNUstep
  mkdir -pv /etc/skel/GNUstep/Defaults >> ${LOG}
  cp -vf ${CWD}/xorg/WindowMaker /etc/skel/GNUstep/Defaults/ >> ${LOG}
  sleep ${SLEEP}

  echo "  Configuring Xterm for future users."
  cp -vf ${CWD}/xorg/Xresources /etc/skel/.Xresources >> ${LOG}
  sleep ${SLEEP}

  if [ ! -z "${USERS}" ]
  then

    for USER in ${USERS}
    do

      if [ -d /home/${USER} ]
      then

        echo "  Configuring Window Maker for user: ${USER}"
        mkdir -pv /home/${USER}/GNUstep/Defaults >> ${LOG}
        cp -vf ${CWD}/xorg/WindowMaker /home/${USER}/GNUstep/Defaults/ >> ${LOG}
        chown -Rv ${USER}:${USER} /home/${USER}/GNUstep >> ${LOG}
        sleep ${SLEEP}

        echo "  Configuring Xterm for user: ${USER}"
        cp -vf ${CWD}/xorg/Xresources /home/${USER}/.Xresources >> ${LOG}
        chown -v ${USER}:${USER} /home/${USER}/.Xresources >> ${LOG}
        sleep ${SLEEP}

      fi

    done

  fi

  echo

}

# Check parameters.
if [[ "${#}" -ne 1 ]]
then

  usage
  exit 1

fi

OPTION="${1}"

case "${OPTION}" in

  --shell) 
    configure_shell
    ;;

  --repos)
    configure_repos
    ;;

  --tools)
    install_tools
    ;;

  --xorg)
    install_wmaker
    ;;

  --setup)
    configure_shell
    configure_repos
    install_tools
    install_wmaker
    ;;

  --help) 
    usage
    exit 0
    ;;

  ?*) 
    usage
    exit 1

esac

exit 0
